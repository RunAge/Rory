// process.env.DEBUG = 'Bot:*';
const debug = require('debug')('Bot:database');
const mongoose = require('mongoose');
const config = require('../config.json');

mongoose.connect(process.env.MONGODB).catch(debug);
const conn = mongoose.connection;

conn.once('open', () => {
  debug('connected');
});

conn.on('disconnect', () => {
  mongoose.connect(config.mongoDB).catch(debug);
});

const userScheme = new mongoose.Schema({
  userID: String, roleID: String, lastseen: String, timestamp: String,
});
const guildScheme = new mongoose.Schema({
  serverID: String, prefix: String, enabled: Boolean, timestamp: String,
});

function getUser(serverID, userID) {
  return mongoose.model(serverID, userScheme).findOne({ userID });
}
function addUser(serverID, userID, roleID) {
  if (!serverID) throw new Error('serverID is undefined!');
  if (!userID || !roleID) throw new Error(`${!userID ? 'userID' : 'roleID'} is undefined!`);
  return mongoose.model(serverID, userScheme).create({
    userID, roleID, timestamp: String(Date.now()),
  });
}

function setupGuild(serverID) {
  if (!serverID) throw new Error('serverID is undefined!');
  return mongoose.model('Guilds', guildScheme).create({
    serverID, prefix: '%', enabled: false, timestamp: String(Date.now()),
  });
}
async function enableGuild(serverID) {
  if (!serverID) throw new Error('serverID is undefined!');
  let updatedGuild;
  try {
    updatedGuild = await mongoose.model('Guilds', guildScheme).findOneAndUpdate({ serverID }, { enabled: true }, {new: true});
  } catch (err) {
    debug(err);
  }
  return updatedGuild;
}
function getGuild(serverID) {
  return mongoose.model('Guilds', guildScheme).findOne({ serverID });
}

async function disableGuild(serverID) {
  if (!serverID) throw new Error('serverID is undefined!');
  let updatedGuild;
  try {
    updatedGuild = await mongoose.model('Guilds', guildScheme).findOneAndUpdate({ serverID }, { enabled: false }, {new: true});
  } catch (err) {
    debug(err);
  }
  return updatedGuild;
}
module.exports = {
  getUser,
  addUser,
  // updateUser,
  enableGuild,
  getGuild,
  setupGuild,
  disableGuild,
};
