const JsonDB = require('../utils/JsonDB.js');
const dotenv = require('dotenv').config();
module.exports = {
  Client: require('./RoryClient'),
  Command: require('./RoryCommand'),
  Config: new JsonDB('../config.json'),
  Database: require('./RoryDatabase'),
};
