const Discord = require('discord.js');
const debugCore = require('debug');
const fs = require('fs');
const path = require('path');
// const Rory = require('./Rory.js');

class RoryClient extends Discord.Client {
  constructor(config, options) {
    super(options);
    this.config = config;
    this.debug = debugCore(`Bot:Rory:${this.shard.id + 1 || 'Non Shard Mode'}/${this.shard.count || ''}`);
    this.commands = new Map();

    this.on('error', this.debug)
      .on('disconnect', () => { process.exit(0); })
      .on('ready', () => {
        this.debug('Ready!');
        if (options.restricted) {
          this.user.setPresence({
            status: 'dnd',
            activity: {
              name: 'RESTRICTED MODE',
            },
          });
        } else {
          this.user.setPresence({
            status: 'online',
            activity: null,
          });
        }
      });

    this.on('message', (message) => {
      if (message.author.id === this.user.id) { return 'Self Message'; }

      if (message.author.bot) { return 'Another bot message, ignored.'; }

      if (message.channel.type !== 'text') { return 'Non "TextChannel" message, ignored.'; }

      if (!message.content.startsWith(this.config.get('prefix'))) { return 'Message without prefix, ignored.'; }

      if (options.debug) { this.debug(`Message: [${message.channel.name}] ${message.author.username}: ${message.content}`); }

      if (options.restricted && options.restricted !== message.guild.id) { return message.reply('Rory operates in restricted mode!'); }

      const args = message.content.split(' ');
      const cmd = args.shift().split(this.config.get('prefix')).join('');

      if (this.commands.has(cmd)) {
        const command = this.commands.get(cmd);

        if (this.validateServer(command, message)) {
          this.debug(this.validateServer(command, message))
          return 'Invalid server';
        }

        if (this.validatePermissins(command, message)) {
          return message.channel.send(`${message.member}, You don't have permissions to use **${command.command}** command!`);
        }

        try {
          command.generator(this, message, args);
        } catch (error) {
          this.debug(`${command.command}, ${error}`);
        }
      }
    });

    this.on('guildMemberAdd', (guildMember) => {
      this.debug(`New User Appears ${guildMember.displayName}`);
      // Rory.Database.registerNewMemeber(guildMember.guild.id, guildMember.id)
    });

    this.login(process.env.TOKEN);
  }

  validatePermissins(command, message) {
    if (command.options) {
      if (command.options.ownerOnly && !this.isOwner(message.author.id)) {
        return true;
      }

      if (command.options.guildOwnerOnly &&
        !this.isGuildOwner(message.author.id, message.guild.ownerID)) {
        return true;
      }

      if (command.options.adminOnly && !this.isAdmin(message.member)) {
        return true;
      }
    }
    return false;
  }

  validateServer(command, message) {
    if (command.options) {
      if (command.options.server && command.options.server !== message.guild.id) {
        return true;
      }
    }
    return false;
  }
  isOwner(id) {
    return this.config.get('owners').includes(String(id));
  }

  isGuildOwner(id, guildOwnerID) {
    return id === guildOwnerID;
  }

  isAdmin(member) {
    return member.hasPermission('ADMINISTRATOR');
  }

  registerCommand(file) {
    if (!file) {
      return this.debug('Commands without files not supported yet!');
    }
    try {
      const Command = require(file);
      const command = new Command();
      return this.commands.set(command.command, command);
    } catch (error) {
      return this.debug(`${file} Errored, ${error}`);
    }
  }

  registerCommandFolder(dirPath) {
    fs.readdirSync(dirPath).forEach((file) => {
      try {
        const CmdFile = require(path.join(dirPath, file));
        const cmd = new CmdFile();
        return this.commands.set(cmd.command, cmd); // TODO: Add path to file
      } catch (error) {
        return this.debug(`${path.join(dirPath, file)} Errored, ${error}`);
      }
    });
  }
  // TODO: Rework this shit
  unregisterCommand(cmd) {
    this.commands.delete(cmd);
    return Promise.resolve(delete require.cache[require.resolve(`../Commands/${cmd}`)]);
  }
  // TODO: Rework this shit
  reregisterCommand(cmd, file) {
    this.unregisterCommand(cmd, file)
      .then(() => {
        this.registerCommand(file);
      });
  }
}


module.exports = RoryClient;
