class RoryCommand {
  constructor(info) {
    this.command = info.command;
    this.description = info.description || `${info.command} don't have description.`;
    this.usage = info.usage || `${info.command} don't have usage example.`;
    this.options = info.options || undefined;
  }

  async generator(bot, message, args, cmd) {
    throw new Error(`${this.command} doesn't have a generator() method.`);
  }
}
module.exports = RoryCommand;
