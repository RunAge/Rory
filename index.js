const Discord = require('discord.js');
const debug = require('debug')('Bot:ShardManager');
const constants = require('./utils/constants.js');

process.env.DEBUG = 'Bot:*';

const shardManager = new Discord.ShardingManager(`${constants.ROOT_DIR}/rory.js`, {
  totalShards: 1,
});

shardManager.on('shardCreate', (shard) => {
  debug(`Launching... ${shard.id + 1}/${shardManager.totalShards}`);
});

shardManager.spawn();
