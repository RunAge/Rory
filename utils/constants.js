// Dirs
const ROOT_DIR = exports.ROOT_DIR = process.cwd();
const UTILS_DIR = exports.UTILS = `${ROOT_DIR}/utils`;
const COMMANDS_DIR = exports.MODULES = `${ROOT_DIR}/Commands`

// Files
const PACKAGE = exports.PACKAGE = `${ROOT_DIR}/package.json`;
const CONFIG = exports.CONFIG = `${ROOT_DIR}/config.json`;
