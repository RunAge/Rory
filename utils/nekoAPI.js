const debug = require('debug')('Bot:Utils:NekoAPI'),
  request = require('request-promise');

const endpoints = {
  'feet': {lewd: true, endpoint: undefined},
  'yuri': {lewd: true, endpoint: undefined},
  'trap': {lewd: true, endpoint: undefined},
  'futanari': {lewd: true, endpoint: undefined},
  'hololewd': {lewd: true, endpoint: undefined},
  'lewdkemo': {lewd: true, endpoint: undefined},
  'solog': {lewd: true, endpoint: undefined},
  'feetg': {lewd: true, endpoint: undefined},
  'cum': {lewd: true, endpoint: undefined},
  'erokemo': {lewd: true, endpoint: undefined},
  'les': {lewd: true, endpoint: undefined},
  'wallpaper': {lewd: false, endpoint: undefined},
  'lewdk': {lewd: true, endpoint: undefined},
  'ngif': {lewd: true, endpoint: undefined},
  'meow': {lewd: false, endpoint: undefined},
  'tickle': {lewd: false, endpoint: undefined},
  'lewd': {lewd: true, endpoint: undefined},
  'feed': {lewd: false, endpoint: undefined},
  'gecg': {lewd: false, endpoint: undefined},
  'eroyuri': {lewd: true, endpoint: undefined},
  'eron': {lewd: true, endpoint: undefined},
  'cum_jpg': {lewd: true, endpoint: undefined},
  'bj': {lewd: true, endpoint: undefined},
  'nsfw_neko_gif': {lewd: true, endpoint: undefined},
  'solo': {lewd: true, endpoint: undefined},
  'kemonomimi': {lewd: false, endpoint: undefined},
  'nsfw_avatar': {lewd: true, endpoint: undefined},
  'gasm': {lewd: false, endpoint: undefined},
  'poke': {lewd: false, endpoint: undefined},
  'anal': {lewd: true, endpoint: undefined},
  'slap': {lewd: false, endpoint: undefined},
  'hentai': {lewd: true, endpoint: undefined},
  'avatar': {lewd: false, endpoint: undefined},
  'erofeet': {lewd: true, endpoint: undefined},
  'holo': {lewd: false, endpoint: undefined},
  'keta': {lewd: true, endpoint: undefined},
  'blowjob': {lewd: true, endpoint: undefined},
  'pussy': {lewd: true, endpoint: undefined},
  'tits': {lewd: true, endpoint: undefined},
  'holoero': {lewd: true, endpoint: undefined},
  'lizard': {lewd: false, endpoint: undefined},
  'pussy_jpg': {lewd: true, endpoint: undefined},
  'pwankg': {lewd: true, endpoint: undefined},
  'classic': {lewd: true, endpoint: undefined},
  'kuni': {lewd: true, endpoint: undefined},
  'waifu': {lewd: false, endpoint: undefined},
  'pat': {lewd: false, endpoint: undefined},
  '8ball': {lewd: false, endpoint: undefined},
  'kiss': {lewd: false, endpoint: undefined},
  'femdom': {lewd: true, endpoint: undefined},
  'neko': {lewd: false, endpoint: undefined},
  'spank': {lewd: true, endpoint: undefined},
  'cuddle': {lewd: false, endpoint: undefined},
  'erok': {lewd: true, endpoint: undefined},
  'fox_girl': {lewd: false, endpoint: undefined},
  'boobs': {lewd: true, endpoint: undefined},
  'Random_hentai_gif': {lewd: true, endpoint: undefined},
  'smallboobs': {lewd: true, endpoint: undefined},
  'hug': {lewd: false, endpoint: undefined},
  'ero': {lewd: true, endpoint: undefined},
  'list': "'feet', 'yuri', 'trap', 'futanari', 'hololewd', 'lewdkemo', 'solog', 'feetg', 'cum', 'erokemo', 'les', 'wallpaper', 'lewdk', 'ngif', 'meow', 'tickle', 'lewd', 'feed', 'gecg', 'eroyuri', 'eron', 'cum_jpg', 'bj', 'nsfw_neko_gif', 'solo', 'kemonomimi', 'nsfw_avatar', 'gasm', 'poke', 'anal', 'slap', 'hentai', 'avatar', 'erofeet', 'holo', 'keta', 'blowjob', 'pussy', 'tits', 'holoero', 'lizard', 'pussy_jpg', 'pwankg', 'classic', 'kuni', 'waifu', 'pat', '8ball', 'kiss', 'femdom', 'neko', 'spank', 'cuddle', 'erok', 'fox_girl', 'boobs', 'Random_hentai_gif', 'smallboobs', 'hug', 'ero'"
}

async function API(endpoint, message) {
  if(!endpoints[endpoint]) return debug(new Error('Undefined endpoint.'))
  if(endpoints[endpoint].lewd && !message.channel.nsfw) {
    return message.reply('This endpoint is restricted to NSFW channels only!')
  }
  if(endpoint === 'list') return message.reply(endpoints['list']);
  const html = await request(`https://nekos.life/api/v2/img/${endpoint}`);
  const json = JSON.parse(html);
  const embed = {
    title: 'Nekos.life Discord. Click me!',
    description: `Requested by **${message.member}**`,
    image: {
      url: json.url
    },
    url: 'https://discord.gg/r4Ju6TJ',
    footer: {
      text: `Command powered by Nekos.life API`
    }
  }
  return message.channel.send(undefined, {embed:embed});
}

module.exports = API;