const fs = require('fs');

class JSONDB {
  /**
   *
   */
  constructor(jsonFile) {
    this.jsonFile = jsonFile;
    this.json = require(this.jsonFile);
  }

  get db() {
    return this.json;
  }

  get(key) {
    if(!key) return console.error('Key is undefined!');
    if(!this.json[key]) return undefined;
    return this.json[key];
  }

  set(key, value) {
    if(!key) return console.error('Key is undefined!');
    this.json[key] = value;
    fs.writeFileSync(this.jsonFile, JSON.stringify(this.json, undefined, 2));
    return this.json;
  }
}
module.exports = JSONDB;
