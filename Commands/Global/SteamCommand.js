const Rory = require(`${process.cwd()}/Bot/Rory`),
      debug = require('debug')('Bot:Command:Steam');


class SteamCommand extends Rory.Command {
  constructor(){
    super({
      command:    'steam',
      description: 'Generate link to open in steam!',
      usage:      `${Rory.Config.get('prefix')}steam[URL]`
    })
  }

  async generator(bot, message, args) {
    if(!args || !args[0]) return message.reply(this.usage);
    message.delete();
    const embed = {
      title: 'Steam link',
      description: `Requested by **${message.member}**`,
      fields: [
        {
          name: 'Open in browser!',
          value: args[0]
        },
        {
          name: 'Open in Steam!',
          value: `steam://openurl/${args[0]}`
        },
      ]
    }
    message.channel.send(undefined, {embed:embed})
  }
}

module.exports = SteamCommand;
