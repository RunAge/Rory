const Rory = require(`${process.cwd()}/Bot/Rory`);
const debug = require('debug')('Bot:Command:Private Roles');

class RolesCommand extends Rory.Command {
  constructor() {
    super({
      command: 'role',
      description: '...',
      usage: 'Its complicated',
    });
  }
  async generator(bot, message, args) {
    if (!args[0]) { return message.reply(`Usage of ${this.command}: ${this.usage}`); }
    const nestedCommands = {
      name: this.name,
      color: this.color,
      restore: this.restore,
    };
    if (!{}.hasOwnProperty.call(nestedCommands, args[0])) { return message.reply(`Usage of ${this.command}: ${this.usage}`); }
    nestedCommands[args[0]](this, message, args);
    return this;
  }

  async name(_super, message, args) {
    const guild = await Rory.Database.getGuild(message.guild.id);
    const user = await Rory.Database.getUser(message.guild.id, String(message.member.id));
    if (!guild) {
      message.reply('Server is not registred, Sorry...');
      throw new Error(`${message.guild.name}[${message.guild.id}] is not registred!`);
    }
    if (!user) {
      message.reply('You don\'t have private role!');
      throw new Error(`${message.member.displayName}, don't have private role!`);
    }
    const role = await message.guild.roles.resolve(user.roleID);
    const nestedCommands = {
      get() {
        return message.reply(`role name is ${role}.`);
      },
      set() {
        const newName = args.slice(2, args.length).join(' ');
        if (newName.length < 5) {
          return message.reply('new role name is too short!');
        }
        role.setName(guild.prefix + newName)
          .then(() => {
            message.reply(`role name updated. ${role}`);
          })
          .catch(err => debug(err));
        return this;
      },
    };
    if (!{}.hasOwnProperty.call(nestedCommands, args[1])) { return message.reply(`Usage of ${_super.command}: ${_super.usage}`); }
    nestedCommands[args[1]](message, args);
    return this;
  }
}

module.exports = RolesCommand;
