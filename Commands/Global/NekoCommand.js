const Rory = require(`${process.cwd()}/Bot/Rory`),
  debug = require('debug')('Bot:Command:Neko');

class NekoCommand extends Rory.Command {
  constructor() {
    super({
      command: 'neko',
      description: 'NEKO!',
      usage: `${Rory.Config.get('prefix')}neko (person)`,
    });
  }
  async generator(bot, message, args) {
    message.reply(`Please use \`${Rory.Config.get('prefix')}nekoAPI neko\``)
  }
}

module.exports = NekoCommand;
