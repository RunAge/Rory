const Rory = require(`${process.cwd()}/Bot/Rory`),
      debug   = require('debug')('Bot:Command:Lewd');

class NekoLewdCommand extends Rory.Command {
  constructor(){
    super({
      command:    'lewd',
      description: 'NEKO!',
      usage:      `${Rory.Config.get('prefix')}lewd (person)`
    })
  }
  async generator(bot, message, args) {
    message.reply(`Please use \`${Rory.Config.get('prefix')}nekoAPI lewd\``)
  }
}

module.exports = NekoLewdCommand;
