const Rory = require(`${process.cwd()}/Bot/Rory`);
const debug = require('debug')('Bot:Command:Register');

class ColorRoleCommand extends Rory.Command {
  constructor() {
    super({
      command: 'color',
      description: 'Change private role color',
      usage: `${Rory.Config.get('prefix')}color (#HEX). ex. ${Rory.Config.get('prefix')}color #FF00FF`,
    });
  }
  async generator(bot, message, args) {
    if (!args[0]) { return message.reply(`Usage of ${this.command}: ${this.usage}`); }
    args[0] = args[0].toUpperCase();
    Rory.Database.getUser(message.guild.id, String(message.member.id))
      .then((user) => {
        if (!user) {
          message.reply('You don\'t have private role!');
          throw new Error(`${message.member.displayName}, don't have private role!`);
        }
        return message.guild.roles.resolve(user.roleID).setColor(args[0], 'Color change!');
      })
      .then((role) => {
        message.reply(`Your new role color ${role}(${args[0]})`);
      })
      .catch(debug);
    return this;
  }
}

module.exports = ColorRoleCommand;
