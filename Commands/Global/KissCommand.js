const Rory = require(`${process.cwd()}/Bot/Rory`),
      debug   = require('debug')('Bot:Command:Kiss');

class KissCommand extends Rory.Command {
  constructor(){
    super({
      command:    'kiss',
      description: 'Kiss!',
      usage:      `${Rory.Config.get('prefix')}kiss (person)`
    })
  }
  async generator(bot, message, args) {
    message.reply(`Please use \`${Rory.Config.get('prefix')}nekoAPI kiss\``)
  }
}

module.exports = KissCommand;
