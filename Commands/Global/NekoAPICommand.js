const Rory = require(`${process.cwd()}/Bot/Rory`),
  debug = require('debug')('Bot:Command:Neko'),
  nekoAPI = require(`${process.cwd()}/utils/nekoAPI.js`)

class NekoAPICommand extends Rory.Command {
  constructor() {
    super({
      command: 'nekoAPI',
      description: 'Random images from Nekos.life!',
      usage: `${Rory.Config.get('prefix')}nekoAPI (endpoint)\nSend \`${Rory.Config.get('prefix')}nekoAPI list\` to get full list of endpoints`
    });
  }
  async generator(bot, message, args) {
    if (!args[0]) { return message.reply(`Usage of ${this.command}: ${this.usage}`); }
    try{
      nekoAPI(args[0], message);
    }
    catch(err) {
      message.reply(err.toString());
      throw new Error(err);
    }
    
  }
}

module.exports = NekoAPICommand;
