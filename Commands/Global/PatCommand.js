const Rory = require(`${process.cwd()}/Bot/Rory`),
      debug   = require('debug')('Bot:Command:Pat');

class PatCommand extends Rory.Command {
  constructor(){
    super({
      command:    'pat',
      description: 'PAT!',
      usage:      `${Rory.Config.get('prefix')}pat (person)`
    })
  }
  async generator(bot, message, args) {
    message.reply(`Please use \`${Rory.Config.get('prefix')}nekoAPI pat\``)
  }
}

module.exports = PatCommand;
