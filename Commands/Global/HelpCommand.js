const Rory = require(`${process.cwd()}/Bot/Rory`);
const debug = require('debug')('Bot:Command:Private Roles');

class HelpCommand extends Rory.Command {
  constructor() {
    super({
      command: 'help',
      description: '...',
      usage: `${Rory.Config.get('prefix')}help`,
    });
  }
  async generator(bot, message) {
    let help = '';
    bot.commands.forEach(command => {
      try {
        help += `**${Rory.Config.get('prefix')}${command.command}** - ${command.description} - ${command.usage}\n`;
      } catch(err) {
        debug(err)
      }
    })
    message.react('🤔')
    message.author.send(help);
    return this;
  }
}

module.exports = HelpCommand;
