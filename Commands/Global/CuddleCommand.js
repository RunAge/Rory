const Rory = require(`${process.cwd()}/Bot/Rory`),
      debug   = require('debug')('Bot:Command:Cuddle');

class CuddleCommand extends Rory.Command {
  constructor(){
    super({
      command:    'cuddle',
      description: 'Cuddle!',
      usage:      `${Rory.Config.get('prefix')}cuddle (person)`
    })
  }
  async generator(bot, message, args) {
    message.reply(`Please use \`${Rory.Config.get('prefix')}nekoAPI cuddle\``)
  }
}

module.exports = CuddleCommand;
