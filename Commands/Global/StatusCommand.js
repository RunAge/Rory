const Rory = require(`${process.cwd()}/Bot/Rory`),
      debug = require('debug')('Bot:Command:Eval'),
      pusage = require('pidusage');


class StatusCommand extends Rory.Command {
  constructor(){
    super({
      command:    'status',
      description: 'Check bot status!',
      usage:      `${Rory.Config.get('prefix')}status`
    })
  }

  async generator(bot, message, args) {
    pusage.stat(process.pid, (err, stat) => {
      let output = {
        title: `${bot.user.username} Status`,
        color: 0xE91E63,
        fields: [
        {
          name: 'Servers',
          value: bot.guilds.array().length,
          inline: true
        },
        {
          name: 'Users',
          value: bot.users.array().length,
          inline: true
        },
        {
          name: 'Cpu usage:',
          value: String(stat.cpu.toFixed(2))+'%',
          inline: true
        },
        {
          name: 'Memory usage:',
          value: String(((stat.memory/1024)/1024).toFixed(2))+'MB',
          inline: true
        }

        ]
      }
      message.channel.send(undefined, {embed:output})
    })

  }
}

module.exports = StatusCommand;
