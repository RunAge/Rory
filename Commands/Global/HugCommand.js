const Rory = require(`${process.cwd()}/Bot/Rory`),
      debug   = require('debug')('Bot:Command:Hug');

class HugCommand extends Rory.Command {
  constructor(){
    super({
      command:    'hug',
      description: 'HUG!',
      usage:      `${Rory.Config.get('prefix')}hug (person)`
    })
  }
  async generator(bot, message, args) {
    message.reply(`Please use \`${Rory.Config.get('prefix')}nekoAPI hug\``)
  }
}

module.exports = HugCommand;
