const Rory = require(`${process.cwd()}/Bot/Rory`);
// const debug = require('debug')('Bot:Command:Eval');

class EvalCommand extends Rory.Command {
  constructor() {
    super({
      command: 'eval',
      description: 'Eval!',
      usage: `${Rory.Config.get('prefix')}eval (code)`,
      options: {
        ownerOnly: true,
      },
    });
  }
  async generator(bot, message, args) {
    const toEval = args.join(' ');
    new Promise((resolve) => {
      resolve(String(eval(toEval)));
    })
      .then((result) => {
        const output = {
          color: 0x67e248,
          author: {
            name: message.member.displayName,
            icon_url: message.author.displayAvatarURL(),
          },
          fields: [
            { name: 'Input', value: toEval },
            { name: 'Output', value: result },
          ],
        };
        message.channel.send('', { embed: output });
      })
      .catch((error) => {
        const output = {
          color: 0xe24848,
          author: {
            name: message.member.displayName,
            icon_url: message.author.displayAvatarURL(),
          },
          fields: [
            { name: 'Input', value: toEval },
            { name: 'Output', value: String(error) },
          ],
        };
        message.channel.send('', { embed: output });
      });
    return this;
  }
}

module.exports = EvalCommand;
