const Rory = require(`${process.cwd()}/Bot/Rory`),
  debug = require('debug')('Bot:Command:ForceRegister');

class ForceRegisterCommand extends Rory.Command {
  constructor() {
    super({
      command: 'forceregister',
      description: 'Make private role!',
      usage: `${Rory.Config.get('prefix')}forceregister @user`,
      options: {
        adminOnly: true,
      },
    });
  }
  async generator(bot, message, args) {
    if (!message.mentions.members.first()) { return message.channel.send(`${message.member} ばか～～、 ${message.member}, ${this.constructor.usage}`); }
    Rory.Database.getUser(message.guild.id, String(message.mentions.members.first().id))
      .then((user) => {
        if (user && user.roleID) {
          message.channel.send(`${message.member} ばか～～、 ${message.mentions.members.first()} already have private role!`);
          throw `User(${message.mentions.members.first().displayName}) already created role!`;
        }
        return user;
      })
      .then(() => message.guild.roles.create({
        data: {
          name: `%${message.mentions.members.first().displayName}`,
          color: 'RANDOM',
          mentionable: true,
          position: 0,
        },
        reason: 'Register command used.',
      }))
      .then((role) => {
        message.channel.send(`${message.author}, ${message.mentions.members.first()} private role is ${role}`);
        return (message.mentions.members.first().roles.add(role), role);
      })
      .then(role => Rory.Database.addUser(message.guild.id, String(message.mentions.members.first().id), String(role.id)))
      .catch(debug);
  }
}

module.exports = ForceRegisterCommand;
