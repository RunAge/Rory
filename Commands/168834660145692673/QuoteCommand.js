const Rory = require(`${process.cwd()}/Bot/Rory`);
const debug = require('debug')('Bot:Command:a1');
const Moment = require('moment');
const quoteChannelID = '300745912571920384';

class Command extends Rory.Command {
  constructor() {
    super({
      command: 'quote',
      description: 'quote [RESTRICTED TO /b/]',
      usage: 'Its complicated',
      options: {
        server: '168834660145692673'
      }
      });
  }
  async generator(bot, message, args) {
    let embedAuthor;

    if(!message.mentions.users.first()){
      embedAuthor = {
        name: args.shift()
      };
    } else {
      args.shift();
      embedAuthor = {
        name: message.guild.members.get(message.mentions.users.first().id).displayName,
        icon_url: message.mentions.users.first().displayAvatarURL || ''
      };
    }

    const output = {
      color: 0x7F00FF,
      author: embedAuthor,
      description: args.join(' '),
      footer: {
        text: `~${new Moment().format('YYYY')}`
      }
    };

    message.guild.channels.get(quoteChannelID).send('',{embed:output});
  }
}

module.exports = Command;
