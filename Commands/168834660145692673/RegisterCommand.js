const Rory = require(`${process.cwd()}/Bot/Rory`),
      debug = require('debug')('Bot:Command:Register');

class RegisterCommand extends Rory.Command {
  constructor(){
    super({
      command:    'register',
      description: 'Make private role!',
      usage:      `${Rory.Config.get('prefix')}register`,
      options: {
        server: '168834660145692673'
      }
    })
  }
  async generator(bot, message, args){
    Rory.Database.getGuild(message.guild.id)
      .then(guild => {
        if(!guild || !guild.enabled){
          message.channel.send(`${message.member} Register is disabled on this server.\nTo enable type \`${Rory.Config.get('prefix')}privateroles on\`**(Server Owner only)**`);
          throw `User(${message.member.displayName}) register is disabled on this server!`;
        }
        return Rory.Database.getUser(message.guild.id, String(message.member.id))
      })
      .then(user => {
        if(user && user.roleID){
          message.channel.send(`${message.member} ばか～～、 You already have private role!`);
          throw `User(${message.member.displayName}) already created role!`;
        }
        return user;
      })
      .then(() => {
        return message.guild.roles.create({
          data: {
            name: '%'+message.member.displayName,
            color: 'RANDOM',
            position: 0
          },
          reason: 'Register command used.'
        });
      })
      .then(role => {
        message.channel.send(`${message.author}, Your private role is ${role}`)
        return(message.member.roles.add(role), role);
      })
      .then((role) => {
        return Rory.Database.addUser(message.guild.id, String(message.member.id), String(role.id));
      })
      .catch(debug)
  }
}

module.exports = RegisterCommand;
