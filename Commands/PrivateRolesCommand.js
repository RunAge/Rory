const Rory = require(`${process.cwd()}/Bot/Rory`);
const debug = require('debug')('Bot:Command:Private Roles');

class PrivateRolesCommand extends Rory.Command {
  constructor() {
    super({
      command: 'privateroles',
      description: 'Toggle private roles on server!',
      usage: `${Rory.Config.get('prefix')}privateroles (on/off)`,
      options: {
        guildOwnerOnly: true,
      },
    });
  }
  async generator(bot, message, args) {
    if (!args[0]) {
      return message.channel.send(`Usage of ${this.command}: ${this.usage}`);
    }
    const guild = await Rory.Database.getGuild(message.guild.id);
    if (!guild) {
      message.reply('Server is not registred, Sorry...');
      throw new Error(`${message.guild.name}[${message.guild.id}] is not registred!`)
    }
    switch (args[0]) {
      case 'on':
        if (guild.enabled) {
          message.channel.send(`${message.member} server had already enabled private roles.\nTo disabled type \`${Rory.Config.get('prefix')}privateroles off\`**(Server Owner only)**`);
          throw new Error(`User(${message.member.displayName}) server had already enabled private roles`);
        }

        message.channel.send(`${message.member} now private roles on servers is enabled.\nTo disabled type \`${Rory.Config.get('prefix')}privateroles off\`**(Server Owner only)**`);
        Rory.Database.enableGuild(message.guild.id);
        break;
      case 'off':
        if (!guild.enabled) {
          message.channel.send(`${message.member} server had already disabled private roles.\nTo enabled type \`${Rory.Config.get('prefix')}privateroles on\`**(Server Owner only)**`);
          throw new Error(`User(${message.member.displayName}) server had already disabled private roles`);
        }

        message.channel.send(`${message.member} now private roles on servers is disabled.\nTo enabled type \`${Rory.Config.get('prefix')}privateroles on\`**(Server Owner only)**`);
        Rory.Database.disableGuild(message.guild.id);
        break;
      default:
        message.channel.send(`Usage of ${this.command}: ${this.usage}`);
        break;
    }
    return this;
  }
}

module.exports = PrivateRolesCommand;
